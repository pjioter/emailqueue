﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmialQueueData.Migrations
{
    public partial class first_migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmialStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmialStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmialSystems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SystemName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmialSystems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmialMessages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmialAdress = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    ReciverId = table.Column<long>(type: "bigint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EmailBody = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EmailStatusId = table.Column<long>(type: "bigint", nullable: false),
                    EmialStatusNavigationId = table.Column<int>(type: "int", nullable: true),
                    EmailSystemId = table.Column<long>(type: "bigint", nullable: false),
                    EmialSystemNavigationId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmialMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmialMessages_EmialStatuses_EmialStatusNavigationId",
                        column: x => x.EmialStatusNavigationId,
                        principalTable: "EmialStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmialMessages_EmialSystems_EmialSystemNavigationId",
                        column: x => x.EmialSystemNavigationId,
                        principalTable: "EmialSystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmialMessages_EmialStatusNavigationId",
                table: "EmialMessages",
                column: "EmialStatusNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_EmialMessages_EmialSystemNavigationId",
                table: "EmialMessages",
                column: "EmialSystemNavigationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmialMessages");

            migrationBuilder.DropTable(
                name: "EmialStatuses");

            migrationBuilder.DropTable(
                name: "EmialSystems");
        }
    }
}
