﻿using EmialQueueData.Models;
using Microsoft.EntityFrameworkCore;

namespace EmialQueueData
{
    public class EmialQueueContext : DbContext
    {
        public EmialQueueContext(DbContextOptions<EmialQueueContext> options) : base(options)
        {
        }

        public DbSet<EmialSystem> EmialSystems { get; set; }

        public DbSet<EmialStatus> EmialStatuses { get; set; }

        public DbSet<EmialMessage> EmialMessages { get; set; }
    }
}