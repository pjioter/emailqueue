﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EmialQueueData.Const;

namespace EmialQueueData.Models
{
    public class EmialMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(450)]
        [Required]
        [RegularExpression(RegularExpression.EmialRegex)]
        public string EmialAdress { get; set; }

        [Required]
        public long ReciverId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string EmailBody { get; set; }

        public long EmailStatusId { get; set; }

        public EmialStatus EmialStatusNavigation { get; set; }

        public long EmailSystemId { get; set; }

        public EmialSystem EmialSystemNavigation { get; set; }

    }
}