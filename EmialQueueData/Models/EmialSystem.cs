﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmialQueueData.Models
{
    public class EmialSystem
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        [Required]
        public string SystemName { get; set; }

        public virtual ICollection<EmialMessage> EmialMessagesNavigation { get; set; }

    }
}