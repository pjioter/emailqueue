﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmialQueueBll.Service.EmialService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace EmialQueueApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class SendEmailsController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public SendEmailsController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        /// <summary>
        /// Endpoint sending emials from given collection by adding it to EmialQeueu
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /addMailToGroup
        ///     [
        ///         {
        ///             "from": "string",
        ///             "to": "string",
        ///             "subject": "string",
        ///             "content": "string",
        ///             "creationDate": "2021-03-17T23:41:56.700Z",
        ///             "reciverId": 0,
        ///             "systemId": 0
        ///          },
        ///          {
        ///             "from": "string",
        ///             "to": "string",
        ///             "subject": "string",
        ///             "content": "string",
        ///             "creationDate": "2021-03-17T23:43:28.704Z",
        ///             "reciverId": 0,
        ///             "systemId": 0
        ///         }
        ///     ]
        /// </remarks>
        /// <param name="emial">collection of emial to send by qeueue</param>
        /// <response code="204">When emial sucesfully added to emialQueue</response>
        /// <response code="400">While error</response>
        /// <response code="401">Unauthorized api user</response>
        [HttpPost("sendEmails")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult Post([FromBody] List<Emial> emial)
        {
            try
            {
                _emailService.EmailCreating(emial);
                return NoContent();

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
