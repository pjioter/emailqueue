﻿namespace EmialQueueBll.Service.EmialService
{
    public class EmialClientConfig
    {
        public string SmtpUser { get; set; }

        public string SmtpPass { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

    }
}