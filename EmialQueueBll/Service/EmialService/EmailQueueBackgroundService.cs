﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EmialQueueBll.Service.EmailClient;
using EmialQueueData.Models;
using EmialQueueData.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EmialQueueBll.Service.EmialService
{
    public class EmailQueueBackgroundService: IHostedService
    {
        private readonly EmailQueue.EmailQueue _tasks;
        private readonly IEmialClient _emialClient;
        private CancellationTokenSource _tokenSource;
        private readonly IServiceScopeFactory scopeFactory;
        private readonly ILogger<EmailQueueBackgroundService> _logger;

        private Task _currentTask;

        public EmailQueueBackgroundService(EmailQueue.EmailQueue tasks, IEmialClient emialClient, IServiceScopeFactory scopeFactory, ILogger<EmailQueueBackgroundService> logger)
        {
            _tasks = tasks;
            _emialClient = emialClient;
            this.scopeFactory = scopeFactory;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(async () =>
            {
                _logger.LogInformation("EmailQueueBackgroundService Hosted Service running.");

                var unitOfWork = scopeFactory.CreateScope().ServiceProvider.GetService<UnitOfWork>();

                _tokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
                while (cancellationToken.IsCancellationRequested == false)
                {
                    try
                    {
                        var taskToRun = _tasks.Dequeue(_tokenSource.Token);

                        unitOfWork.GetRepository<EmialMessage>().Insert(taskToRun);
                        unitOfWork.Save();
                        _emialClient.Send(taskToRun, unitOfWork);
                    }
                    catch (OperationCanceledException)
                    {
                        _logger.LogError("Cancelaction Task Error");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }
                }
            });
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _tokenSource.Cancel(); // cancel "waiting" for task in blocking collection

            if (_currentTask == null) return;

            // wait when _currentTask is complete
            await Task.WhenAny(_currentTask, Task.Delay(-1, cancellationToken));
        }
    }
}