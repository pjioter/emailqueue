﻿using System;

namespace EmialQueueBll.Service.EmialService
{
    public class Emial
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }

        public long ReciverId { get; set; }

        public long SystemId { get; set; }

    }
}