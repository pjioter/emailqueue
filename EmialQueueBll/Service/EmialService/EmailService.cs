﻿using System.Collections.Generic;
using EmialQueueData.Models;

namespace EmialQueueBll.Service.EmialService
{
    public class EmailService: IEmailService
    {
        private readonly EmailQueue.EmailQueue _emailQueue;

        public EmailService(EmailQueue.EmailQueue emailQueue)
        {
            _emailQueue = emailQueue;
        }

        public void EmailCreating(List<Emial> emials)
        {
            foreach (var emial in emials)
            {
                _emailQueue.Enqueue(new EmialMessage()
                {
                    EmailBody = emial.Content,
                    Title = emial.Subject,
                    EmailSystemId = emial.SystemId,
                    EmialAdress = emial.To,
                    ReciverId = emial.ReciverId,
                });
            }
        }
    }
}