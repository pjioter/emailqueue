﻿using System.Collections.Concurrent;
using System.Threading;
using EmialQueueData.Models;

namespace EmialQueueBll.Service.EmailQueue
{
    public class EmailQueue
    {
        private readonly BlockingCollection<EmialMessage> _tasks;

        public EmailQueue() => _tasks = new BlockingCollection<EmialMessage>();

        public void Enqueue(EmialMessage email) => _tasks.Add(email);

        public EmialMessage Dequeue(CancellationToken token) => _tasks.Take(token);
    }
}