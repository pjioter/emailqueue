﻿using EmialQueueData.Models;
using EmialQueueData.Utilities;

namespace EmialQueueBll.Service.EmailClient
{
    public interface IEmialClient
    {
        void Send(EmialMessage emialData, UnitOfWork unitOfWork);

    }
}