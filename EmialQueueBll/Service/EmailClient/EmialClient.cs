﻿using EmialQueueBll.Enums;
using EmialQueueBll.Service.EmialService;
using EmialQueueData.Models;
using EmialQueueData.Utilities;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace EmialQueueBll.Service.EmailClient
{
    public class EmialClient: IEmialClient
    {
        private readonly EmialClientConfig _emialClientConfig;

        public EmialClient(IOptions<EmialClientConfig> emialClientConfig)
        {
            _emialClientConfig = emialClientConfig.Value;
        }

        public void Send(EmialMessage emialData, UnitOfWork unitOfWork)
        {
            // create message
            var email = new MimeMessage();

            //todo
            email.From.Add(MailboxAddress.Parse(emialData.EmialAdress));
            email.To.Add(MailboxAddress.Parse(emialData.EmialAdress));
            email.Subject = emialData.Title;
            email.Body = new TextPart(TextFormat.Html) {Text = emialData.EmailBody};

            // send email
            using (var smtp = new SmtpClient())
            {
                var emailDb = unitOfWork.GetRepository<EmialMessage>().GetByID(emialData.Id);
                smtp.Connect(_emialClientConfig.SmtpHost, _emialClientConfig.SmtpPort, SecureSocketOptions.StartTls);
                smtp.Authenticate(_emialClientConfig.SmtpUser, _emialClientConfig.SmtpPass);
                smtp.Send(email);
                smtp.Disconnect(true);
                emailDb.EmailStatusId = (long)EmialStatusEnum.Sended;
            }
        }
    }
}